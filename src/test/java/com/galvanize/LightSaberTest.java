package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LightSaberTest {
    LightSaber lightSaber;
    @BeforeEach
    void setUp() {
        lightSaber = new LightSaber(12345);
    }

    @Test
    public void constructorShouldReturnSerialNumber() {
        LightSaber testSaber = new LightSaber(987654321);
        long expected = 987654321L;
        long actual = testSaber.getJediSerialNumber();

        assertEquals(expected, actual);
    }
    @Test
    public void shouldGetCharge() {
        float expected = 100.0f;
        float actual = lightSaber.getCharge();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldSetCharge() {
        lightSaber.setCharge(80f);
        float expected = 80f;
        float actual = lightSaber.getCharge();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldGetColor() {
        String expected = "green";
        String actual = lightSaber.getColor();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldSetColor() {
        lightSaber.setColor("purple");
        String expected = "purple";
        String actual = lightSaber.getColor();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldGetSerialNumber() {
        long expected = 12345L;
        long actual = lightSaber.getJediSerialNumber();

        assertEquals(expected, actual);
    }

    @Test
    public void useShouldDecreaseCharge() {
        lightSaber.use(5.0f);
        float expected = 100.0f - ((10.0f / 60.0f) * 5.0f);
        float actual = lightSaber.getCharge();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldGetRemainingMinutes() {
        float expected = 300f;
        float actual = lightSaber.getRemainingMinutes();

        assertEquals(expected, actual);
    }

    @Test
    public void rechargeShouldReturnFull() {
        lightSaber.setCharge(0f);
        assertEquals(0f, lightSaber.getCharge());

        lightSaber.recharge();
        assertEquals(100f, lightSaber.getCharge());
    }

}